(function() {
    angular.module('App')
        .controller('EnrolledStudentsCtrl', [MyKendo]);

    function MyKendo() {
        function onChange(e) {
            var rows = e.sender.select();
            rows.each(function(e) {
                var grid = $("#grid").data("kendoGrid");
                var dataItem = grid.dataItem(this);
                console.log(dataItem.Info);
                $("#window").kendoWindow({
                    height: 65,
                    width: 300,
                    position: {
                        top: 200,
                        left: "42.5%"
                    },
                    title: "Students"
                });
                var myWindow = $("#window").data("kendoWindow");
                myWindow.open();
                myWindow.content(dataItem.Info);
            })
        }

        $("#grid").kendoGrid({
            dataSource: {
                type: "json",
                data: arr,
                pageSize: 10,
                schema: {
                    model: {
                        fields: {
                            StudentName: {
                                type: "string"
                            },
                            FatherName: {
                                type: "string"
                            },
                            RollNO: {
                                type: "numeric"
                            },
                            Department: {
                                type: "string"
                            }
                        }
                    }
                },
            },
            change: onChange,
            selectable: "row",
            columns: [{
                field: "StudentName",
                title: "Student Name",
                
            }, {
                field: "FatherName",
                title: "Father Name",
                
            }, {
                field: "RollNO",
                title: "Roll NO",
               
            }, {
                field: "Department",
                title: "Department",
                
            }],
            scrollable: true,
            pageable: {
                pageSizes: [1, 2, 3]
            }
        })

        document.querySelectorAll('.k-pager-numbers-wrap').forEach(function(a) {
            a.remove()
        })
    }
})();