(function() {
    angular.module("App").config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('Home', {
                url: "/",
                templateUrl: "app/home/home.html"
            })

        .state('Students Info', {
            url: "/SI",
            templateUrl: "app/student-info/student.info.html",
            controller: 'StudentsinfoCtrl'
        })

        .state('Enrolled Students', {
            url: "/ES",
            templateUrl: "app/Enrolled-student/Enrolled.student.html",
            controller: 'EnrolledStudentsCtrl'
        })

        $urlRouterProvider.otherwise('/');
    });
})();